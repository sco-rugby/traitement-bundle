<?php

namespace ScoRugby\TraitementBundle\Manager;

use ScoRugby\TraitementBundle\Model\Traitement;
use ScoRugby\API\Manager\AbstractAPIManager;

/**
 * Description of TraitementManager
 *
 * @author Antoine BOUET
 */
class TraitementManager extends AbstractAPIManager implements TraitementManagerInterface {

    public function getClassName(): string {
        return Traitement::class;
    }

    public function getResourceName(): string {
        return 'Traitement';
    }
}
