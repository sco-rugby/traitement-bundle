<?php

namespace ScoRugby\TraitementBundle\Model;

/**
 *
 * @author Antoine BOUET
 */
trait TraitementTrait {

    protected ?string $id = null;
    protected string $source;
    protected string $key;
    protected string $job;
    protected ?string $description = null;
    protected array $informations = [];
    protected array $context = [];

    public function getId(): ?string {
        return $this->id;
    }

    public function getSource(): string {
        return $this->source;
    }

    public function getKey(): string {
        return $this->key;
    }

    public function getDescription(): ?string {
        return $this->description;
    }

    public function setDescription(?string $description): self {
        $this->description = $description;

        return $this;
    }

    public function getInformations(): ?array {
        return $this->informations;
    }

    public function addInformation(string $information): void {
        $this->informations[] = $information;
    }

    public function setInformations(array $informations): self {
        $this->informations = $informations;

        return $this;
    }

    public function getContext(): ?array {
        return $this->context;
    }

    public function addContext(string $key, string $context): void {
        $this->context[$key] = $context;
    }

    public function setContext(array $context): self {
        $this->context = $context;

        return $this;
    }

    public function __toString(): string {
        return sprintf('%s::%s-%s', $this->getSource(), $this->getJob(), $this->getKey());
    }
}
