<?php

namespace ScoRugby\TraitementBundle\Model;

class Traitement implements TraitementInterface, \Stringable {

    use TraitementTrait;

    public function __construct(protected string $source, protected string $key, protected string $job, protected ?string $id = null) {
        if (null === $this->id) {
            $this->id = uniqid();
        }
    }

    public function getJob(): string {
        return $this->job;
    }
}
