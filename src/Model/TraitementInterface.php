<?php

namespace ScoRugby\TraitementBundle\Model;

interface TraitementInterface {

    public function getId(): ?string;

    public function getSource(): string;

    public function getKey(): string;

    public function getJob(): string;

    public function getDescription(): ?string;

    public function setDescription(?string $description): self;

    public function setInformations(array $informations): self;

    public function addInformation(string $information): void;

    public function getInformations(): ?array;

    public function getContext(): ?array;

    public function addContext(string $key, string $context): void;

    public function setContext(array $context): self;
}
