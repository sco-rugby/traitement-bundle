<?php

namespace ScoRugby\TraitementBundle\Service;

use ScoRugby\TraitementBundle\Model\TraitementInterface;

interface TraitementServiceInterface {

    public function init(): void;

    public function shutdown(): void;

    public function getTraitement(): ?TraitementInterface;
}
