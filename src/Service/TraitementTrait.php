<?php

namespace ScoRugby\TraitementBundle\Service;

use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use ScoRugby\TraitementBundle\Model\Traitement;
use ScoRugby\TraitementBundle\Model\TraitementInterface;
use ScoRugby\TraitementBundle\Event\TraitementEvent;

trait TraitementTrait {

    protected ?TraitementEvent $event = null;
    protected EventDispatcherInterface $dispatcher;

    public function initProcess(string $source, string $key, string $job, string $id = null, array $context = []): void {
        $this->event = new TraitementEvent((new Traitement($source, $key, $job, $id))->setContext($context));
        $this->dispatcher->dispatch($this->event, TraitementEvent::INIT);
    }

    public function startProcess(?int $steps = null): void {
        if (null !== $steps) {
            $this->event->addContext['steps'] = $steps;
        }
        $this->dispatcher->dispatch($this->event, TraitementEvent::START);
    }

    public function endProcess(): void {
        $this->dispatcher->dispatch($this->event, TraitementEvent::END);
    }

    public function getTraitement(): ?TraitementInterface {
        return $this->event->getTraitement();
    }

    public function setSuccess(string|array $message = []): void {
        if (is_string($message)) {
            $this->getTraitement()->addInformation($message);
        } else {
            foreach ($message as $msg) {
                $this->getTraitement()->addInformation($msg);
            }
        }
        $this->dispatcher->dispatch($this->event, TraitementEvent::SUCCESS);
    }

    public function setError(string $message = null, string $method = null): void {
        if (null !== $message) {
            $this->getTraitement()->addInformation($message);
        }
        if (null !== $method) {
            $this->getTraitement()->addContext('method', $method);
        }
        $this->dispatcher->dispatch($this->event, TraitementEvent::ERROR);
    }

    public function setFailure(string $message): void {
        $this->getTraitement()->addInformation($message);
        $this->dispatcher->dispatch($this->event, TraitementEvent::FAILURE);
    }
}
